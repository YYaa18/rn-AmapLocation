/**
 * Created by leilong on 2018/4/4 0004.
 * QQ 1148030615
 */
package com.AmapLocation;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.location.AMapLocationClientOption.AMapLocationProtocol;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.location.AMapLocationQualityReport;


import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.WritableMap;

import android.widget.Toast;

import android.os.Bundle;


public class LocationModule extends ReactContextBaseJavaModule {

	private AMapLocationClient locationClient = null;
	private AMapLocationClientOption locationOption = null;
	private ReactApplicationContext mreactContext = null;
	private Callback msuccessCallback = null;


	public LocationModule(ReactApplicationContext reactContext) {
		super(reactContext);
		mreactContext = reactContext;
		initLocation();
	}

	@Override
	public String getName() {
		return "LocationModule";
	}

	/**
	 * 初始化定位
	 *
	 * @since 2.8.0
	 * @author hongming.wang
	 *
	 */
	private void initLocation(){
		//初始化client
		locationClient = new AMapLocationClient(mreactContext);
		locationOption = getDefaultOption();
		//设置定位参数
		locationClient.setLocationOption(locationOption);
		// 设置定位监听
		locationClient.setLocationListener(locationListener);
	}

	/**
	 * 默认的定位参数
	 * @since 2.8.0
	 * @author hongming.wang
	 *
	 */
	private AMapLocationClientOption getDefaultOption(){
		AMapLocationClientOption mOption = new AMapLocationClientOption();
		mOption.setLocationMode(AMapLocationMode.Hight_Accuracy);//可选，设置定位模式，可选的模式有高精度、仅设备、仅网络。默认为高精度模式
		mOption.setGpsFirst(false);//可选，设置是否gps优先，只在高精度模式下有效。默认关闭
		mOption.setHttpTimeOut(30000);//可选，设置网络请求超时时间。默认为30秒。在仅设备模式下无效
		mOption.setInterval(2000);//可选，设置定位间隔。默认为2秒
		mOption.setNeedAddress(true);//可选，设置是否返回逆地理地址信息。默认是true
		mOption.setOnceLocation(false);//可选，设置是否单次定位。默认是false
		mOption.setOnceLocationLatest(false);//可选，设置是否等待wifi刷新，默认为false.如果设置为true,会自动变为单次定位，持续定位时不要使用
		AMapLocationClientOption.setLocationProtocol(AMapLocationProtocol.HTTP);//可选， 设置网络请求的协议。可选HTTP或者HTTPS。默认为HTTP
		mOption.setSensorEnable(false);//可选，设置是否使用传感器。默认是false
		mOption.setWifiScan(true); //可选，设置是否开启wifi扫描。默认为true，如果设置为false会同时停止主动刷新，停止以后完全依赖于系统刷新，定位位置可能存在误差
		mOption.setLocationCacheEnable(true); //可选，设置是否使用缓存定位，默认为true
		return mOption;
	}


	/**
	 * 定位监听
	 */
	AMapLocationListener locationListener = new AMapLocationListener() {
		@Override
		public void onLocationChanged(AMapLocation location) {
			WritableMap params = Arguments.createMap();
			if (null != location) {

				//errCode等于0代表定位成功，其他的为定位失败，具体的可以参照官网定位错误码说明
				if(location.getErrorCode() == 0){

					params.putBoolean("successful",true);
					params.putDouble("lng", location.getLongitude());
					params.putDouble("lat", location.getLatitude());
					params.putInt("type", location.getLocationType());
					params.putDouble("jindu", location.getAccuracy());
					params.putString("Country", location.getCountry());
					params.putString("Province", location.getProvince());
					params.putString("City", location.getCity());
					params.putString("CityCode", location.getCityCode());
					params.putString("District", location.getDistrict());
					params.putString("AdCode", location.getAdCode());
					params.putString("Dress", location.getAddress());
					if(msuccessCallback != null){
						msuccessCallback.invoke(params);
					}


				} else {
					//定位失败
					params.putBoolean("successful",false);
					params.putInt("ErrorCode",location.getErrorCode());
					params.putString("ErrorInfo",location.getErrorInfo());
					params.putString("LocationDetail",location.getLocationDetail());
				}
				params.putBoolean("WifiOpend",location.getLocationQualityReport().isWifiAble());
				params.putString("GPSStatus",getGPSStatusString(location.getLocationQualityReport().getGPSStatus()));
				params.putInt("GPSNumber",location.getLocationQualityReport().getGPSSatellites());
				if(msuccessCallback != null){
					msuccessCallback.invoke(params);
				}

			} else {
				params.putBoolean("successful",false);
				if(msuccessCallback != null){
					msuccessCallback.invoke(params);
				}
			}
		}
	};


	/**
	 * 获取GPS状态的字符串
	 * @param statusCode GPS状态码
	 * @return
	 */
	private String getGPSStatusString(int statusCode){
		String str = "";
		switch (statusCode){
			case AMapLocationQualityReport.GPS_STATUS_OK:
				str = "GPS状态正常";
				break;
			case AMapLocationQualityReport.GPS_STATUS_NOGPSPROVIDER:
				str = "手机中没有GPS Provider，无法进行GPS定位";
				break;
			case AMapLocationQualityReport.GPS_STATUS_OFF:
				str = "GPS关闭，建议开启GPS，提高定位质量";
				break;
			case AMapLocationQualityReport.GPS_STATUS_MODE_SAVING:
				str = "选择的定位模式中不包含GPS定位，建议选择包含GPS定位的模式，提高定位质量";
				break;
			case AMapLocationQualityReport.GPS_STATUS_NOGPSPERMISSION:
				str = "没有GPS定位权限，建议开启gps定位权限";
				break;
		}
		return str;
	}


	/**
	 * 开始定位
	 *
	 * @since 2.8.0
	 * @author hongming.wang
	 *
	 */
	@ReactMethod
	public void startLocation(Callback successCallback){
		msuccessCallback = successCallback;
		// 设置定位参数
		locationClient.setLocationOption(locationOption);
		// 启动定位
		locationClient.startLocation();
	}

	/**
	 * 停止定位
	 *
	 * @since 2.8.0
	 * @author hongming.wang
	 *
	 */
	@ReactMethod
	public void stopLocation(){
		// 停止定位
		locationClient.stopLocation();
	}

	/**
	 * 销毁定位
	 *
	 * @since 2.8.0
	 * @author hongming.wang
	 *
	 */
	@ReactMethod
	public void destroyLocation(){
		if (null != locationClient) {
			/**
			 * 如果AMapLocationClient是在当前Activity实例化的，
			 * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
			 */
			locationClient.onDestroy();
			locationClient = null;
			locationOption = null;
		}
	}

}




