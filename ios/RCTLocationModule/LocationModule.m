//
//  RCTLocationModule.m
//  RCTLocationModule
//
//  Created by 雷龙 on 2018/4/4.
//  Copyright © 2018年 雷龙. All rights reserved.
//

#import "LocationModule.h"
#define DefaultLocationTimeout  6
#define DefaultReGeocodeTimeout 3
@implementation LocationModule

RCT_EXPORT_MODULE();
RCT_EXPORT_METHOD(startLocation){
    [AMapServices sharedServices].apiKey =@"你的高德KEY";
//    [self configLocationManager];
//    [self startSerialLocation];
    
    self.locationManager = [[AMapLocationManager alloc] init];
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyHundredMeters];
    //   定位超时时间，最低2s，此处设置为2s
    self.locationManager.locationTimeout =2;
    //   逆地理请求超时时间，最低2s，此处设置为2s
    self.locationManager.reGeocodeTimeout = 2;
    
    [self.locationManager requestLocationWithReGeocode:YES completionBlock:^(CLLocation *location, AMapLocationReGeocode *regeocode, NSError *error) {
        
        if (error)
        {
            NSLog(@"locError:{%ld - %@};", (long)error.code, error.localizedDescription);
            NSDictionary *str =@{@"successfun": @(NO),
                                 @"ErrorCode" : @(error.code),
                                 @"code" : error.localizedDescription
                                 };
            [self sendEventWithName:@"AmapLoactionEvent" body:str];
            if (error.code == AMapLocationErrorLocateFailed)
            {
                return;
            }
        }
        if (regeocode)
        {
            NSDictionary *str =@{@"successful": @(YES),
                                 @"Dress" : regeocode.formattedAddress,
                                 @"lng" : @(location.coordinate.longitude),
                                 @"lat" : @(location.coordinate.latitude),
                                 @"Country" : regeocode.country,
                                 @"Province" : regeocode.province,
                                 @"City" : regeocode.city,
                                 @"CityCode" : regeocode.citycode,
                                 @"District" : regeocode.district,
                                 @"Street" : regeocode.street
                                 };
            [self sendEventWithName:@"AmapLoactionEvent" body:str];
        }
    }];
}
- (NSArray<NSString *> *)supportedEvents
{
    return @[@"AmapLoactionEvent"];
}
@end
