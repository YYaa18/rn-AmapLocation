//
//  RCTLocationModule.h
//  RCTLocationModule
//
//  Created by 雷龙 on 2018/4/4.
//  Copyright © 2018年 雷龙. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTLog.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <React/RCTEventDispatcher.h>
#import <React/RCTEventEmitter.h>
@interface LocationModule : RCTEventEmitter <RCTBridgeModule,AMapLocationManagerDelegate>
    @property AMapLocationManager *locationManager;
    
@end
