import { NativeModules,NativeEventEmitter,Platform } from 'react-native';

class AmapLocation {
    constructor(){
        const calendarManagerEmitter = new NativeEventEmitter(NativeModules.LocationModule);

        const subscription = calendarManagerEmitter.addListener(
            'AmapLoactionEvent',
            (reminder) => this.callback(reminder)
        );

    }
    LocationModule = NativeModules.LocationModule


    startLocation(fun){

        if(Platform.OS === 'ios'){
            this.callback = fun;
            this.LocationModule.startLocation();
        }else{
            this.LocationModule.startLocation((e)=>{
                fun(e);
            });
        }


    }
}


//export default NativeModules.LocationModule;

export default  AmapLocation;

